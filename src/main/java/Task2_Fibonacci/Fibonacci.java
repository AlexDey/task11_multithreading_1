package Task2_Fibonacci;

/**
 * Fibonacci sequence
 */
public class Fibonacci extends Thread {
    private int n;

    public Fibonacci(int n) {
        this.n = n;
    }

    public void run() {
        for (int i = 1; i <= n; i++) {
            System.out.println(currentThread().getName() + " " + fibonacci(i));
        }
    }

    private static long fibonacci(int i) {
        if (i <= 1) return i;
        else return fibonacci(i - 1) + fibonacci(i - 2);
    }
}
