package Task2_Fibonacci;

import java.util.Random;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Create a task that produces a sequence of n Fibonacci numbers, where n is provided
 * to the constructor of the task. Create a number of these tasks and drive them using
 * threads.
 */
public class Application {

    public static void main(String[] args) {
        // the number of threads
        int threadsNumber = 3;
        // create Fibonacci sequence with a length from 0 to 10 elements
        for (int i = 1; i <= threadsNumber; i++) {
            new Fibonacci(new Random().nextInt(10)).start();
        }
    }
}
