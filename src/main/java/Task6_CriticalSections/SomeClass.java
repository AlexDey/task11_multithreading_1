package Task6_CriticalSections;

/**
 * Blueprint for creating objects
 */
public class SomeClass extends Thread {

    private synchronized void firstMethod() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            System.out.println("firstMethod " + i);
            sleep(500);
        }
    }

    private synchronized void secondMethod() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            System.out.println("secondMethod " + i);
            sleep(500);
        }
    }

    private synchronized void thirdMethod() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            System.out.println("thirdMethod " + i);
            sleep(500);
        }
    }

    public void run() {
        try {
            firstMethod();
            secondMethod();
            thirdMethod();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
