package Task1_PingPong;

public class MyThread extends Thread {
    private String action;
    private PlayerOptions playerOptions;

    MyThread(String action, PlayerOptions playerOptions) {
        this.action = action;
        this.playerOptions = playerOptions;
        setName(action);
        start();
    }

    public void run() {
        int count = 1;
        if (action.equals("Hit the ball")) {
            try {
                while (true) {
                    System.out.println("Set " + count++ + " started");
                    playerOptions.hitBall();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (action.equals("Catch the ball")) {
            try {
                while (true) {
                    playerOptions.catchBall();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
