package Task7_CommunicationPipe;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Write program in which two tasks use a pipe to communicate
 */
public class Application {
    public static void main(String[] args) throws IOException {

        final PipedOutputStream output = new PipedOutputStream();
        final PipedInputStream input = new PipedInputStream(output);

        Thread thread1 = new Thread(() -> {
            try {
                output.write("Hello world, pipe!".getBytes());
            } catch (IOException e) {
                System.out.println("Something goes wrong1!");
            }
        });

        Thread thread2 = new Thread(() -> {
            try {
                int data = input.read();
                while (data != -1) {
                    System.out.print((char) data);
                    data = input.read();
                }
            } catch (IOException e) {
                System.out.println("Something goes wrong2!");
            }
        });

        thread1.start();
        thread2.start();
    }
}
