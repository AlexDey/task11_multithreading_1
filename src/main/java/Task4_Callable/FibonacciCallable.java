package Task4_Callable;

import java.util.concurrent.Callable;

/**
 * Fibonacci sequence
 */
public class FibonacciCallable implements Callable {
    private int n;

    FibonacciCallable(int n) {
        this.n = n;
    }

    private static long fibonacci(int i) {
        if (i <= 1) return i;
        else return fibonacci(i - 1) + fibonacci(i - 2);
    }

    @Override
    public Object call() {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += fibonacci(i);
        }
        return sum;
    }
}
