package Task4_Callable;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Modify Exercise 2 so that the task is a Callable that sums the values of all the Fibonacci
 * numbers. Create several tasks and display the results.
 */
public class Application {

    public static void main(String[] args) {
        FibonacciCallable fibonacciCallable = new FibonacciCallable(10);
        System.out.println("The sum of Fibonacci sequence is: "
                + fibonacciCallable.call());
    }
}
