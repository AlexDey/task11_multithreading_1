package Task5_ScheduledThreadPool;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Create a task that sleeps for a random amount of time between 1 and 10 seconds,
 * then displays its sleep time and exits. Create and run a quantity (given on the command
 * line) of these tasks. Do it by using ScheduledThreadPool.
 */
public class Application {
    public static void main(String[] args) {
        Runnable runnable = () -> System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now());
        int poolSize = Integer.parseInt(args[0]);
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(poolSize);
        for (int i = 1; i <= poolSize; i++) {
            int delay = getRandomInt();
            ses.schedule(new Thread(runnable), delay, TimeUnit.SECONDS);
        }
    }

    private static int getRandomInt() {
        return new Random().nextInt(10);
    }
}
