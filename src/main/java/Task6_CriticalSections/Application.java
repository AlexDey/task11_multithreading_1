package Task6_CriticalSections;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Create a class with three methods containing critical sections that all synchronize on
 * the same object. Create multiple tasks to demonstrate that only one of these methods
 * can run at a time. Now modify the methods so that each one synchronizes on a different
 * object and show that all three methods can be running at once.
 */
public class Application {

    public static void main(String[] args) {
        for (int i = 0; i<3; i++) {
            new SomeClass().start();
        }
    }
}
