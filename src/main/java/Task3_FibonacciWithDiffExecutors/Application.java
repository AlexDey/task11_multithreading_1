package Task3_FibonacciWithDiffExecutors;

import Task2_Fibonacci.Fibonacci;

import java.util.concurrent.*;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Repeat Fibonacci exercise using the different types of executors
 */
public class Application {

    public static void main(String[] args) {
        runViaExecutorInterface();
        runViaExecutorService();
        runViaScheduledExService();
    }

    private static void runViaExecutorInterface() {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Fibonacci(8));
    }

    private static void runViaExecutorService() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new Fibonacci(5));
    }

    private static void runViaScheduledExService() {
        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.schedule(new Fibonacci(10), 2, TimeUnit.SECONDS);
    }
}
