package Task1_PingPong;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 18 Dec 2018
 * Task 1 : Write simple “ping-pong” program using wait() and notify()
 */
public class Application {

    public static void main(String[] args) throws InterruptedException {
        PlayerOptions playerOptions = new PlayerOptions();
        new MyThread("Hit the ball", playerOptions);
        new MyThread("Catch the ball", playerOptions);
    }
}
