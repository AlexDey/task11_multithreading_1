package Task1_PingPong;

class PlayerOptions {
    private String currentOption = "";

    synchronized void hitBall() throws InterruptedException {
        currentOption = "Hit the ball";
        System.out.println(currentOption);
        notify();
        wait();
    }

    synchronized void catchBall() throws InterruptedException {
        currentOption = "Catch the ball";
        System.out.println(currentOption);
        wait(1000);
        notify();
        wait();
    }
}
